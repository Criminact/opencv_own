#include <iostream>
#include "include/opencv/cv.h"
#include "include/opencv2/opencv.hpp"
#include "include/opencv/highgui.h"


using namespace std;
using namespace cv;

const int position_max = 1;
int position_min;
Mat img;


void classic(int position,void *);

int main(int argc,char *argv[]){

img = imread("corridor.jpg");
namedWindow("Window");
imshow("Window",img);

position_min =0;
createTrackbar("Switch","Window",&position_min,position_max,classic);

while(char(waitKey(1) != 'q'));

	return 0;

}

void classic(int position,void *){
Mat imgcvt;
if(position > 0)
{
	cvtColor(img,imgcvt,CV_BGR2GRAY);

}
else
{
	imgcvt = img;
}

imshow("Window",imgcvt);
}