#include <iostream>
#include "include/opencv/cv.h"
#include "include/opencv2/opencv.hpp"
#include "include/opencv/highgui.h"


using namespace std;
using namespace cv;

const int position_max=1;
int position;

void classic(int position,void *);
int main(int argc,char *argv[]){

Mat img(200,200,CV_8UC3,Scalar(0,0,0));

namedWindow("win");
imshow("win",img);
position =0;


createTrackbar("track","win",&position,position_max,classic);

while(char(waitKey(1) != 'q'));
	return 0;
}


void classic(int position,void *){
Mat imgcvt1;
Mat imgcvt2;
if(position > 0)
{
	Mat imgcvt1(200,200,CV_8UC3,Scalar(255,0,0));
	imshow("win",imgcvt1);
}
else
{
	Mat imgcvt2(200,200,CV_8UC3,Scalar(0,0,255));
	imshow("win",imgcvt2);
}

}