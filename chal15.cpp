#include <iostream>
#include "include/opencv/cv.h"
#include "include/opencv2/opencv.hpp"
#include "include/opencv/highgui.h"

using namespace std;
using namespace cv;

void CallBackFunc1(int event, int x, int y, int flags, void* userdata);
void CallBackFunc2(int event, int x, int y, int flags, void* userdata);
void classic();
int main(){

Mat img(200,200,CV_8UC3,Scalar(0,0,0));
Point p1(100,100);

line(img,p1,p1,Scalar(255,0,0),10);

for(int i=5;i<40;i++)
{
	for(int j=5;j<195;j++)
	{
		img.at<Vec3b>(i,j)[0] = 123;
		img.at<Vec3b>(i,j)[1] = 240;
		img.at<Vec3b>(i,j)[2] = 150;
	}
}
Point p2(10,20);
putText(img,"Click me!",p2,FONT_HERSHEY_SCRIPT_SIMPLEX,0.6,Scalar(0,0,0),2,1);

namedWindow("win");
setMouseCallback("win", CallBackFunc1, NULL);
imshow("win",img);
waitKey(0);


	return 0;
}
void CallBackFunc1(int event, int x, int y, int flags, void* userdata)
{
if(event == EVENT_LBUTTONDOWN){
	if(x > 5 && x < 195 &&  y > 5 && y < 40){
		classic();
	}
}

}
void classic()
{
Mat imgcvt(200,200,CV_8UC3,Scalar(0,0,0));
Point p1(100,100);

line(imgcvt,p1,p1,Scalar(0,255,0),10);

for(int i=5;i<40;i++)
{
	for(int j=5;j<195;j++)
	{
		imgcvt.at<Vec3b>(i,j)[0] = 123;
		imgcvt.at<Vec3b>(i,j)[1] = 240;
		imgcvt.at<Vec3b>(i,j)[2] = 150;
	}
}
Point p2(10,20);
putText(imgcvt,"Click me!",p2,FONT_HERSHEY_SCRIPT_SIMPLEX,0.6,Scalar(0,0,0),2,1);
namedWindow("win");
setMouseCallback("win", CallBackFunc2, NULL);
imshow("win",imgcvt);
waitKey(0);
}
void CallBackFunc2(int event, int x, int y, int flags, void* userdata)
{
if(event == EVENT_LBUTTONDOWN){
	if(x > 5 && x < 195 &&  y > 5 && y <  40){
		main();
	}
}

}
