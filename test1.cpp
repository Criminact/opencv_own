#include <iostream>
#include "include/opencv/cv.h"
#include "include/opencv2/opencv.hpp"
#include "include/opencv/highgui.h"


using namespace std;
using namespace cv;

int main(int argc,char *argv[]){

Mat img(400,400,CV_8UC3,Scalar(45,89,230));

Point point1(10,10);
Point point2(10,100);
Point point3(10,50);
Point point4(50,50);
Point point5(50,10);
Point point6(75,10);
Point point7(125,10);
Point point8(125,100);
Point point9(175,10);
Point point10(125,100);
Point point11(175,100);
Point point12(75,100);
Point point13(200,10);
Point point14(200,100);
Point point15(200,50);
Point point16(200,10);
Point point17(250,50);
Point point18(250,10);
Point point19(300,10);
Point point20(260,100);
Point point21(340,100);
Point point22(280,60);
Point point23(320,60);
Point point24(150,200);
Point point25(150,290);
Point point26(200,245);
Point point27(110,180);
Point point28(280,320);



Scalar colorLine(200,13,60);
int thickness = 4;
int radius = 40;
int line_type = 6;
int shift = 0;

line(img,point1,point2,colorLine,thickness);
line(img,point3,point4,colorLine,thickness);
line(img,point1,point5,colorLine,thickness);
line(img,point6,point7,colorLine,thickness);
line(img,point7,point8,colorLine,thickness);
line(img,point7,point9,colorLine,thickness);
line(img,point10,point11,colorLine,thickness);
line(img,point12,point10,colorLine,thickness);
line(img,point13,point14,colorLine,thickness);
line(img,point15,point17,colorLine,thickness);
line(img,point13,point18,colorLine,thickness);
line(img,point19,point20,colorLine,thickness);
line(img,point19,point21,colorLine,thickness);
line(img,point22,point23,colorLine,thickness);
line(img,point24,point25,colorLine,thickness);
circle(img,point26,radius,colorLine,thickness,line_type,shift);
rectangle(img,point27,point28,colorLine,thickness,line_type,shift);

namedWindow("4.challenge");
imshow("4.challenge",img);
waitKey(0);

	return 0;

}