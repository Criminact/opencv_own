#include <iostream>
#include "include/opencv/cv.h"
#include "include/opencv2/opencv.hpp"
#include "include/opencv/highgui.h"


using namespace std;
using namespace cv;

void classic();
void CallBackFunc(int event, int x, int y, int flags, void* userdata);
int main(int argc,char *argv[])
{

Mat img = imread("mini.jpg",CV_LOAD_IMAGE_COLOR);


for(int i=10;i<60;i++)
{
	for(int j=10;j<90;j++)
	{
		img.at<Vec3b>(i,j)[0] = 255;
		img.at<Vec3b>(i,j)[1] = 255;
		img.at<Vec3b>(i,j)[2] = 255;
	}
}

Point p(10,40);
putText(img,"Click me!",p,FONT_HERSHEY_SCRIPT_SIMPLEX,0.6,Scalar(0,0,0),2,1);


namedWindow("window");
setMouseCallback("window", CallBackFunc, NULL);
imshow("window",img);
waitKey(0);

	return 0;
}
void CallBackFunc(int event, int x, int y, int flags, void* userdata)
{
    if  ( event == EVENT_LBUTTONDOWN )
    {
        cout << "Left button of the mouse is clicked - position (" << x << ", " << y << ")" << endl;
    	if( x > 10 && x < 90 && y > 10  && y < 60)
    		{	classic();
    			cout << "classic fxn called";
    		}
    }
       
}
void classic()
{
Mat imgcvt = imread("mini.jpg",CV_LOAD_IMAGE_GRAYSCALE);

imshow("window",imgcvt);
}