#include <iostream>
#include "include/opencv/cv.h"
#include "include/opencv2/opencv.hpp"
#include "include/opencv/highgui.h"


using namespace std;
using namespace cv;

Mat img1,img2,img3,img4;
int position1 = 0;
const int position_max1 = 3;
int position2 = 0;
const int position_max2 = 2;

void filter1(int position1,void *);
void filter2(int position2,void *);
int main(int argc,char * argv[])
{


namedWindow("win");
createTrackbar("DOPEY1","win",&position1,position_max1,filter1);
createTrackbar("DOPEY2","win",&position2,position_max2,filter2);
waitKey(0);

	return 0;
}
void filter1(int position1,void *){
	img1 = imread("mini.jpg");
	img2 = imread("mini.jpg");
	img3 = imread("mini.jpg");
	img4 = imread("mini.jpg");
	Mat gray,edge1,edge2,edge3,edge4;
	cvtColor(img1,gray,CV_BGR2GRAY);

	Canny(gray,edge1,50,50,3);
	Canny(gray,edge2,50,100,3);
	Canny(gray,edge3,50,150,3);
	Canny(gray,edge4,50,200,3);


for(int i=0;i<img4.rows;i++){
	for(int j=0;j<img4.cols;j++){

			if(img4.at<Vec3b>(i,j)[0] < 80  && img4.at<Vec3b>(i,j)[1] < 80  && img4.at<Vec3b>(i,j)[2] < 80)
			{
				img4.at<Vec3b>(i,j)[0] = 20;
				img4.at<Vec3b>(i,j)[0] = 120;
				img4.at<Vec3b>(i,j)[0] = 70;	
			}

	}
}

for(int i=0;i<img2.rows/8;i++){
	for(int j=0;j<img2.cols;j++){

			if(img2.at<Vec3b>(i,j)[0] < 80  && img2.at<Vec3b>(i,j)[1] < 80  && img2.at<Vec3b>(i,j)[2] < 80)
			{
				img2.at<Vec3b>(i,j)[0] = 20;
				img2.at<Vec3b>(i,j)[0] = 10;
				img2.at<Vec3b>(i,j)[0] = 80;	
			}

	}
}

for(int i=0;i<img3.rows/2;i++){
	for(int j=0;j<img3.cols/2;j++){

			if(img3.at<Vec3b>(i,j)[0] < 80  && img3.at<Vec3b>(i,j)[1] < 80  && img3.at<Vec3b>(i,j)[2] < 80)
			{
				img3.at<Vec3b>(i,j)[0] = 30;
				img3.at<Vec3b>(i,j)[0] = 70;
				img3.at<Vec3b>(i,j)[0] = 50;	
			}

	}
}

for(int i=0;i<gray.rows/2;i++){
	for(int j=0;j<gray.cols/2;j++){

			if(gray.at<Vec3b>(i,j)[0] < 80  && gray.at<Vec3b>(i,j)[1] < 80  && gray.at<Vec3b>(i,j)[2] < 80)

			{
				gray.at<Vec3b>(i,j)[0] = 30;
				gray.at<Vec3b>(i,j)[0] = 70;
				gray.at<Vec3b>(i,j)[0] = 50;	
			}

	}
}

for(int i=0;i<edge1.rows/2;i++){
	for(int j=0;j<edge1.cols/2;j++){

			if(edge1.at<Vec3b>(i,j)[0] < 80  && edge1.at<Vec3b>(i,j)[1] < 80  && edge1.at<Vec3b>(i,j)[2] < 80)

			{
				edge1.at<Vec3b>(i,j)[0] = 30;
				edge1.at<Vec3b>(i,j)[0] = 70;
				edge1.at<Vec3b>(i,j)[0] = 50;	
			}

	}
}
Mat imgcvt;

switch(position1)
{
case 0:
imgcvt = edge1;
break;

case 1:
imgcvt = edge2;
break;

case 2:
imgcvt = edge3;
break;

case 3:
imgcvt = edge4;
break;

	default:
	cout << "NOT FOUND\n";
}
imshow("win",imgcvt);
}
void filter2(int position2,void *){
	img1 = imread("mini.jpg");
	img2 = imread("mini.jpg");
	img3 = imread("mini.jpg");
	img4 = imread("mini.jpg");
	Mat gray,edge1,edge2,edge3,edge4;
	cvtColor(img1,gray,CV_BGR2GRAY);

	Canny(gray,edge1,50,50,3);
	Canny(gray,edge2,50,100,3);
	Canny(gray,edge3,50,150,3);
	Canny(gray,edge4,50,200,3);


for(int i=0;i<img4.rows;i++){
	for(int j=0;j<img4.cols;j++){

			if(img4.at<Vec3b>(i,j)[0] < 80  && img4.at<Vec3b>(i,j)[1] < 80  && img4.at<Vec3b>(i,j)[2] < 80)
			{
				img4.at<Vec3b>(i,j)[0] = 20;
				img4.at<Vec3b>(i,j)[0] = 120;
				img4.at<Vec3b>(i,j)[0] = 70;	
			}

	}
}

for(int i=0;i<img2.rows/8;i++){
	for(int j=0;j<img2.cols;j++){

			if(img2.at<Vec3b>(i,j)[0] < 80  && img2.at<Vec3b>(i,j)[1] < 80  && img2.at<Vec3b>(i,j)[2] < 80)
			{
				img2.at<Vec3b>(i,j)[0] = 20;
				img2.at<Vec3b>(i,j)[0] = 10;
				img2.at<Vec3b>(i,j)[0] = 80;	
			}

	}
}

for(int i=0;i<img3.rows/2;i++){
	for(int j=0;j<img3.cols/2;j++){

			if(img3.at<Vec3b>(i,j)[0] < 80  && img3.at<Vec3b>(i,j)[1] < 80  && img3.at<Vec3b>(i,j)[2] < 80)
			{
				img3.at<Vec3b>(i,j)[0] = 30;
				img3.at<Vec3b>(i,j)[0] = 70;
				img3.at<Vec3b>(i,j)[0] = 50;	
			}

	}
}

for(int i=0;i<gray.rows/2;i++){
	for(int j=0;j<gray.cols/2;j++){

			if(gray.at<Vec3b>(i,j)[0] < 80  && gray.at<Vec3b>(i,j)[1] < 80  && gray.at<Vec3b>(i,j)[2] < 80)

			{
				gray.at<Vec3b>(i,j)[0] = 30;
				gray.at<Vec3b>(i,j)[0] = 70;
				gray.at<Vec3b>(i,j)[0] = 50;	
			}

	}
}

for(int i=0;i<edge1.rows/2;i++){
	for(int j=0;j<edge1.cols/2;j++){

			if(edge1.at<Vec3b>(i,j)[0] < 80  && edge1.at<Vec3b>(i,j)[1] < 80  && edge1.at<Vec3b>(i,j)[2] < 80)

			{
				edge1.at<Vec3b>(i,j)[0] = 30;
				edge1.at<Vec3b>(i,j)[0] = 70;
				edge1.at<Vec3b>(i,j)[0] = 50;	
			}

	}
}
Mat imgcvt;

switch(position2)
{
case 0:
imgcvt = img2;
break;

case 1:
imgcvt = img3;
break;

case 2:
imgcvt = img4;
break;

case 3:
imgcvt = gray;
break;

	default:
	cout << "NOT FOUND\n";
}
imshow("win",imgcvt);
}