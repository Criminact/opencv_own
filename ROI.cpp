#include <iostream>
#include "include/opencv/opencv.hpp"

using namespace std;
using namespace cv;

bool ldown=false,lup=false;
Mat img;
Point corner1,corner2;
Rect box;

void call_back(int event,int x,int y,int,void *){
if(event == EVENT_LBUTTONDOWN){
ldown = true;
corner1.x = x;
corner1.y = y;
cout << "corner 1 recorded " << corner1 << endl;
}

if(event == EVENT_LBUTTONUP){
lup = true;
corner2.x = x;
corner2.y = y;
cout << "corner 2 recorded " << corner2 << endl;
}

if(ldown == false || lup == false){
cout << "PLEASE SELECT A REGION\n";
}

if(ldown == true && lup == true){
box.width = abs(corner1.x-corner2.x);
box.height = abs(corner1.y-corner2.y);
box.x = min(corner1.x,corner2.x);
box.y = min(corner1.y,corner2.y);

Mat image(img,box);
namedWindow("cropped");
imshow("cropped",image);
ldown = false;
lup = false;
}
}

int main(int argc,char* args[]){
img = imread(args[1],CV_LOAD_IMAGE_COLOR);
namedWindow("window");
imshow("window",img);
setMouseCallback("window",call_back);
waitKey(0);

	return 0;
}

