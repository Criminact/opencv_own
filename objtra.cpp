#include "include/opencv2/opencv.hpp"
#include "include/opencv2/tracking/tracker.hpp"
#include "include/opencv2/tracking/show.hpp"

 
using namespace cv;
using namespace std;
 
int main(int argc, char **argv)
{
    // Set up tracker. 
    // Instead of MIL, you can also use 
    // BOOSTING, KCF, TLD, MEDIANFLOW or GOTURN  
    Ptr<Tracker> tracker = Tracker::create( "MIL" );
 
    VideoCapture cap(0); // open the default camera
    if(!cap.isOpened())  // check if we succeeded
        return -1;
 
    while(1)
    {
        Mat frame1,frame2;
	
	cap >> frame1;         // get a new frame from camera
        cvtColor(frame1,frame2,CV_RGB2GRAY);
        
     
    // Read first frame. 
    Mat frame;
    cap.read(frame);
 
    // Define an initial bounding box
    Rect2d bbox(287, 23, 86, 320);
 
    // Uncomment the line below if you 
    // want to choose the bounding box
    // bbox = selectROI(frame, false);
     
    // Initialize tracker with first frame and bounding box
    tracker->init(frame, bbox);
 
    while(video.read(frame))
    {
        // Update tracking results
        tracker->update(frame, bbox);
 
        // Draw bounding box
        rectangle(frame, bbox, Scalar( 255, 0, 0 ), 2, 1 );
 
        // Display result
        imshow("Tracking", frame);
        int k = waitKey(1);
        if(k == 27) break;
 
    }
 
    return 0; 
     
}
