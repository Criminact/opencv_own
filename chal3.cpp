#include <iostream>
#include "include/opencv/cv.h"
#include "include/opencv2/opencv.hpp"
#include "include/opencv/highgui.h"


using namespace std;
using namespace cv;
void classic();
void one_chan();
void three_chan();
void four_chan();
int main(int argc,char * argv[]){
classic();

	return 0;
}
void classic(){

int channels;
cout << "Here we create 8 bit unsigned image so please give pixel value between 0-255\n";
cout << "Press 1 to create a single channel image\n";
cout << "Press 3 to create a 3 channel image\nand 4 to create a 4 channel image\n";
cin >> channels;
switch(channels){
case 1:
one_chan();

case 3:
three_chan();

case 4:
four_chan();

	default:
	classic();
}
int choice;
cout << "Want to create one more image??\npress 5 to do it now\nand ctrl+c to exit\n";
cin >> choice;
if(choice == 5){classic();};
}

void one_chan()
{

int value;
	cout << "Give pixel value to create a image\n";
	cin >> value;
	if(value > 255){cout << "Error as told above 8 bit unsigned imge must have pixel value less than 255\nhere we will take your value as 255\n";}

Mat img(500,500,CV_8UC1,Scalar(value));

namedWindow("3.CHALLENGE",CV_WINDOW_AUTOSIZE);
imshow("3.CHALLENGE",img);
waitKey(0);

}

void three_chan()
{
int value1;
int value2;
int value3;
	cout << "Give pixel value to create a image\n value 1.";
	cin >> value1;
	if(value1 > 255){cout << "Error as told above 8 bit unsigned imge must have pixel value less than 255\nhere we will take your value as 255\n";}
	cout << "value 2.\n";
	cin >> value2;
	if(value2 > 255){cout << "Error as told above 8 bit unsigned imge must have pixel value less than 255\nhere we will take your value as 255\n";}
	cout << "value 3.\n";
	cin >> value3;
	if(value3 > 255){cout << "Error as told above 8 bit unsigned imge must have pixel value less than 255\nhere we will take your value as 255\n";}

Mat img(500,500,CV_8UC3,Scalar(value1,value2,value3));

namedWindow("3.CHALLENGE",CV_WINDOW_AUTOSIZE);
imshow("3.CHALLENGE",img);
waitKey(0);

}

void four_chan()
{
int value1;
int value2;
int value3;
int value4;

	cout << "Give pixel value to create a image\n value 1.";
	cin >> value1;
	if(value1 > 255){cout << "Error as told above 8 bit unsigned imge must have pixel value less than 255\nhere we will take your value as 255\n";}
	cout << "value 2.\n";
	cin >> value2;
	if(value2 > 255){cout << "Error as told above 8 bit unsigned imge must have pixel value less than 255\nhere we will take your value as 255\n";}
	cout << "value 3.\n";
	cin >> value3;
	if(value3 > 255){cout << "Error as told above 8 bit unsigned imge must have pixel value less than 255\nhere we will take your value as 255\n";}
	cout << "value 4.\n";
	cin >> value4;
	if(value4 > 255){cout << "Error as told above 8 bit unsigned imge must have pixel value less than 255\nhere we will take your value as 255\n";}

Mat img(500,500,CV_8UC4,Scalar(value1,value2,value3,value4));

namedWindow("3.CHALLENGE",CV_WINDOW_AUTOSIZE);
imshow("3.CHALLENGE",img);
waitKey(0);

}
