#include <iostream>
#include "include/opencv/cv.h"
#include "include/opencv2/opencv.hpp"
#include "include/opencv/highgui.h"


using namespace std;
using namespace cv;

int main(int argc,char *argv[]){

Mat img(100,100,CV_8UC3,Scalar(0,0,0));

Point point1(50,50);
Point point2(50,50);


Scalar colorLine(0,0,255);
int thickness = 4;

line(img,point1,point2,colorLine,thickness);

namedWindow("4.challenge img1");
imshow("4.challenge img1",img);

for(int i=0;i<img.rows;i++){
	for(int j=0;j<img.cols;j++)
		{
		if(img.at<Vec3b>(i,j)[0] < 20 && img.at<Vec3b>(i,j)[1] < 20 && img.at<Vec3b>(i,j)[2] > 200)
	        img.at<Vec3b>(i,j)[0] = 255;
	    	img.at<Vec3b>(i,j)[1] = 0;
	    	img.at<Vec3b>(i,j)[2] = 0;
		}
}




namedWindow("4.challenge img2");
imshow("4.challenge img2",img);
waitKey(0);

	return 0;

}