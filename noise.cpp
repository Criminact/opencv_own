#include <iostream>
#include "include/opencv2/opencv.hpp"
#include "include/opencv/cv.h"


using namespace std;
using namespace cv;

int removelight(Mat img,Mat pattern,int method);
int main(int argc,char *argv[]){


    VideoCapture cap(0); // open the default camera
    if(!cap.isOpened())  // check if we succeeded
        return -1;
 
    while(1)
    {
        Mat img,frame,img_noise;
	
	cap >> img;         // get a new frame from camera
        cvtColor(img,frame,CV_RGB2GRAY);
        
namedWindow("orngl-gray",CV_WINDOW_AUTOSIZE);

imshow("orngl-gray",img);

medianBlur(frame,img_noise,3);

namedWindow("blurred",CV_WINDOW_AUTOSIZE);

imshow("blurred",img_noise);

removelight(img,img_noise,1);

}

waitKey(0);



  return 0;
}
int removelight(Mat img,Mat pattern,int method)
{
Mat aux1;

Mat img32,pattern32;
img.convertTo(img32,CV_32F);
pattern.convertTo(pattern32,CV_32F);
aux1 = 1 - (img32/pattern32) ;
aux1 = aux1*255;
aux1.convertTo(aux1,CV_8U);

Mat aux2;

aux2 = pattern - img;

namedWindow("divison",CV_WINDOW_AUTOSIZE);
imshow("divison",aux1);

namedWindow("difference",CV_WINDOW_AUTOSIZE);
imshow("difference",aux2);


 return 0;
}
