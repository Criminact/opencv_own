#include <iostream>
#include "include/opencv/opencv.hpp"
#include <vector>
#include <math.h>

using namespace std;
using namespace cv;

int main()
{
Mat image = imread("shapes.jpg"),gray,final;
vector<Vec2f> lines;
cvtColor(image,gray,CV_BGR2GRAY);
imshow("as",gray);
HoughLines(gray,lines,1,CV_PI/180.f,1100);

for(int i=0;i<lines.size();i++){
float rho = lines[i][0];
float theta = lines[i][1];

double a = cos(theta),b = sin(theta);
double x0 = a * rho, y0 = b * rho;
Point pt1(cvRound(x0+1000*(-b)),cvRound(y0+1000*(a)));
Point pt2(cvRound(x0-1000*(-b)),cvRound(y0-1000*(a)));
line(image,pt1,pt2,Scalar(255,0,0));
}

imshow("final",image);
if(char(waitKey(0) == 's')){
imwrite("saved2.jpg",final);
cout << "saved\n";
}

	return 0;
}
