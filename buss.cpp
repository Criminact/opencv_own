#include <iostream>
#include "include/opencv/opencv.hpp"

using namespace std;
using namespace cv;


int main(){
Mat frame1 = imread("raj.jpg");
Mat frame2 = imread("dog.jpg");

Mat maino,hide;
resize(frame1,maino,Size(300,300),1,1,INTER_LINEAR);
resize(frame2,hide,Size(300,300),1,1,INTER_LINEAR);

imshow("CARRIER",maino);
imshow("KEY",hide);

imwrite("nonstegged1.png",maino);
imwrite("nonstegged2.png",hide);

if(maino.type() != hide.type() || maino.size() != hide.size())
{
printf("Given Image types are different \n");
return -1;
}

Mat final(maino.rows, maino.cols, maino.type());

Mat Front_image, Hidden_image;

Mat front_mask(maino.rows, maino.cols, maino.type(), Scalar(0xF0, 0xF0, 0xF0));
Mat hidden_mask(hide.rows, hide.cols, hide.type(), Scalar(0xF0, 0xF0, 0xF0));

bitwise_and(maino, front_mask, Front_image);
bitwise_and(hide, hidden_mask, Hidden_image);

for(int j = 0; j <maino.rows; j++){
	for(int i = 0; i <maino.cols; i++){
		Hidden_image.at<Vec3b>(j,i)[0] = Hidden_image.at<Vec3b>(j,i)[0] >> 4;
		Hidden_image.at<Vec3b>(j,i)[1] = Hidden_image.at<Vec3b>(j,i)[1] >> 4;
		Hidden_image.at<Vec3b>(j,i)[2] = Hidden_image.at<Vec3b>(j,i)[2] >> 4;
					}
}
	bitwise_or(Front_image, Hidden_image, final);

resize(final,final,Size(300,300),1,1,INTER_LINEAR);
imshow("FINAL OUTPUT",final);
imwrite("stegged.png",final);
waitKey(0);
}
