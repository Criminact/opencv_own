#include <iostream>
#include "include/opencv/cv.h"
#include "include/opencv2/opencv.hpp"
#include "include/opencv/highgui.h"


using namespace std;
using namespace cv;

int main(int argc,char * argv[]){

Mat img = imread("corridor.jpg",CV_LOAD_IMAGE_COLOR);

for(int i=0;i<img.rows;i++){
	for(int j=0;j<img.cols;j++){

 img.at<Vec3b>(i,j)[0] = 255-img.at<Vec3b>(i,j)[0];
 img.at<Vec3b>(i,j)[1] = 255-img.at<Vec3b>(i,j)[1];
 img.at<Vec3b>(i,j)[2] = 255-img.at<Vec3b>(i,j)[2];
	}
}


namedWindow("win");
imshow("win",img);
waitKey(0);


	return 0;
}