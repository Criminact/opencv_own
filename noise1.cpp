#include <iostream>
#include "include/opencv2/opencv.hpp"
#include "include/opencv/cv.h"

using namespace std;
using namespace cv;

int blur(Mat img);
int removelight(Mat img,Mat pattern,int);

int main(int argc,char *argv[]){
Mat img = imread(argv[1],CV_LOAD_IMAGE_GRAYSCALE);
blur(img);

  return 0;
}


int blur(Mat img){
Mat img_noise;
medianBlur(img,img_noise,3);
namedWindow("IMAGE",CV_WINDOW_AUTOSIZE);
imshow("IMAGE",img_noise);
waitKey(0);

int a = 1;
int b = 0; 

removelight(img,img_noise,a);

  return 0;

}




int removelight(Mat img,Mat pattern,int o)
{

Mat aux;

if(o == 1){
Mat img32,pattern32;
img.convertTo(img32,CV_32F);
pattern.convertTo(pattern32,CV_32F);
aux = 1 - (img32/pattern32) ;
aux = aux*255;
aux.convertTo(aux,CV_8U);
namedWindow("difference",CV_WINDOW_AUTOSIZE);
imshow("difference",aux);
}

else{
aux = pattern - img;
namedWindow("division",CV_WINDOW_AUTOSIZE);
imshow("division",aux);
}

waitKey(0);

 return 0;
}


