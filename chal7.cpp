#include <iostream>
#include "include/opencv/cv.h"
#include "include/opencv2/opencv.hpp"
#include "include/opencv/highgui.h"

using namespace std;
using namespace cv;

int main(int argc,char * argv[]){

Mat img1 = imread("mini.jpg",CV_LOAD_IMAGE_COLOR);
Mat img2 = imread("mini.jpg",CV_LOAD_IMAGE_GRAYSCALE);

Mat img3(500,500,CV_8UC3,Scalar(0,0,0));

for(int i=0;i<img3.rows;i++){
	for(int j=0;j<img3.cols;j++)
	{
		img3.at<Vec3b>(i,j)[0] = img1.at<Vec3b>(i,j)[0]-img2.at<Vec3b>(i,j)[0];
		img3.at<Vec3b>(i,j)[1] = img1.at<Vec3b>(i,j)[1]-img2.at<Vec3b>(i,j)[1];
		img3.at<Vec3b>(i,j)[2] = img1.at<Vec3b>(i,j)[2]-img2.at<Vec3b>(i,j)[2];
	}
}



namedWindow("aft");
imshow("aft",img3);
waitKey(0);

	return 0;
}