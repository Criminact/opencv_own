#include <iostream>
#include "include/opencv/opencv.hpp"
#include <vector>

using namespace std;
using namespace cv;

int main()
{
Mat image = imread("circle.jpeg"),gray,final;
resize(image,final,Size(640,480),1,1,CV_INTER_LINEAR);
vector<Vec3f> circles;
cvtColor(final,gray,CV_BGR2GRAY);
HoughCircles(gray,circles,CV_HOUGH_GRADIENT,1,10,100,37,5);

for(int i=0;i<circles.size();i++){
Point center(cvRound(circles[i][0]),cvRound(circles[i][1]));
int radius = cvRound(circles[i][2]);
circle(final,center,3,Scalar(0,0,255),-1);
circle(final,center,radius,Scalar(255,0,0),3,2,0);
}

imshow("final",final);
if(char(waitKey(0) == 's')){
imwrite("saved.jpg",final);
cout << "saved\n";
}

	return 0;
}
