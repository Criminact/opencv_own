#include <iostream>
#include "include/opencv/cv.h"
#include "include/opencv2/opencv.hpp"
#include "include/opencv/highgui.h"
#include <vector>

using namespace std;
using namespace cv;

vector<vector<Point> >contours;
vector<Vec4i>hercy;

int main()
{
Mat img1 = imread("shapes.jpg");
cvtColor(img1,img1,CV_BGR2GRAY);
medianBlur(img1,img1,3);
Mat img2;
threshold(img1,img2,60,255,CV_THRESH_BINARY);
findContours(img2,contours,hercy,RETR_EXTERNAL,CHAIN_APPROX_NONE);
 RNG rng(12345);

Mat drawing = Mat::zeros(img2.size(),CV_8UC3);
    for(int i=0;i<contours.size();i++)
    {
        Scalar color=Scalar(rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
        drawContours(drawing,contours, i,color, 2, 8, hercy, 0, Point() );
    }     
 

namedWindow("shape");
imshow("shape",drawing);

waitKey(0);

	return 0;
}