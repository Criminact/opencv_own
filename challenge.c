#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include "include/opencv/cv.h"
#include "include/opencv/highgui.h"


int main(int argc,char *argv[])
{
IplImage* img = 0; 
  int height,width,step,channels;
  uchar *data;
  int i,j,k;

  if(argc<2){
    printf("Usage: main <image-file-name>\n\7");
    exit(0);
  }

  // load an image  
  img=cvLoadImage("mini.jpg",1);
  if(!img){
    printf("Could not load image file: %s\n",argv[1]);
    exit(0);
  }

  // get the image data
  height    = img->height;
  width     = img->width;
  step      = img->widthStep;
  channels  = img->nChannels;
  data      = (uchar *)img->imageData;
  printf("Processing a %dx%d image with %d channels\n",height,width,channels); 

 IplImage * gray = cvCreateImage(cvGetSize(img),IPL_DEPTH_8U,1);
 cvCvtColor(img,gray,CV_RGB2GRAY);
 
 cvShowImage("hey_gray_scaled", gray); 

 for(int i=0;i<gray->height;i++){
	uchar* ptr = (uchar*)(gray->imageData + i * gray->widthStep);
for(int y=0;y<gray->width;y++){
 
	if(ptr[3*y+1] > 100){ptr[3*y+1] = 255;}
	if(ptr[3*y+1] > 100){ptr[3*y+1] = 255;}	
//here if a pixel value is greater than 100 make it 255 
}
}
	

 //save the image
 cvSaveImage("gray_scale.jpg", gray);

 // show the image
  cvShowImage("input_gray_scaled", img);  
  cvShowImage("output_gray_scaled", gray);
	
  // wait for a key
  cvWaitKey(0);

  // release the image
  cvReleaseImage(&img);
  cvReleaseImage(&gray);



return 0;
}
