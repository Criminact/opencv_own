#include <iostream>
#include "include/opencv/opencv.hpp"

using namespace std;
using namespace cv;


int main()
{

Mat image = imread("stegged.png");

int numberRows = image.rows;
int numberCols = image.cols;

Mat FrontImage(numberRows, numberCols, image.type());
Mat HiddenImage(numberRows, numberCols, image.type());

Mat front_mask(numberRows, numberCols, image.type(), Scalar(0xF0, 0xF0, 0xF0));
Mat hidden_mask(numberRows, numberCols, image.type(), Scalar(0x0F, 0x0F, 0x0F));

bitwise_and(image, front_mask, FrontImage);
bitwise_and(image, hidden_mask, HiddenImage);

for(int j = 0; j <numberRows; j++)
{
for(int i = 0; i <numberCols; i++){
HiddenImage.at<Vec3b>(j,i)[0] = HiddenImage.at<Vec3b>(j,i)[0] << 4;
HiddenImage.at<Vec3b>(j,i)[1] = HiddenImage.at<Vec3b>(j,i)[1] << 4;
HiddenImage.at<Vec3b>(j,i)[2] = HiddenImage.at<Vec3b>(j,i)[2] << 4;
}
}

imshow("was hidden",HiddenImage);
imshow("was carrier",FrontImage);
waitKey(0);

return 0;
}
