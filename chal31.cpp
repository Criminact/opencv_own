#include <iostream>
#include "include/opencv/cv.h"
#include "include/opencv2/opencv.hpp"
#include "include/opencv/highgui.h"
#include <vector>

using namespace std;
using namespace cv;

vector<vector<Point2f> >contours;
vector<Vec4i>hercy;

int main(int,char**)
{
VideoCapture cap("frogger.mp4");
if(!cap.isOpened())
{
	cout << "Video file not readable\n";
}

for(;;)
{

	Mat main_file,save;
	
	cap >> main_file;
	cvtColor(main_file,main_file,CV_BGR2GRAY);
	
	cap.read(save);
	
	imwrite("tuna.jpg",save);
	Mat bg_image = imread("tuna.jpg",CV_LOAD_IMAGE_GRAYSCALE);
	Mat final;
	
	absdiff(main_file,bg_image,final);
	medianBlur(final,final,5);

int erosion_size = 5;
int dilation_size = 5;

Mat element1 = getStructuringElement(MORPH_RECT,Size(2*erosion_size+1,2*erosion_size+1),Point(erosion_size,erosion_size));
Mat element2 = getStructuringElement(MORPH_RECT,Size(2*dilation_size+1,2*dilation_size+1),Point(dilation_size,dilation_size));

//opening of file
erode(final,final,element1);
dilate(final,final,element2);

	
	threshold(final,final,8,255,CV_THRESH_BINARY);



  	findContours(final,contours,hercy,RETR_EXTERNAL,CHAIN_APPROX_NONE);
 
 int largest_area=1500;

 vector<Rect>bounding_rect(0);
 vector<int>contours_indx(0);

 for(int i=0;i<contours.size();i++)
 {
 	double a=contourArea(contours[i],false);
 	if(a > largest_area)
 	{
 		
 		contours_indx.push_back(i);
 		bounding_rect.push_back(boundingRect(contours[i]));
 	
 	}
 }
 
 Scalar color(255,0,255);
 
 for(int i=0;i==bounding_rect.size();i++)
{
 	drawContours(final,contours,contours_indx[i],color,CV_FILLED,8,hercy);
 	rectangle(main_file,bounding_rect[i],Scalar(255,0,255),2,8,0);
}


	imshow("win",main_file);

	if(waitKey(24) >= 0) break;

}

	return 0;
}