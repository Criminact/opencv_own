#include <iostream>
#include "include/opencv/cv.h"
#include "include/opencv2/opencv.hpp"
#include "include/opencv/highgui.h"


using namespace std;
using namespace cv;

Mat img;
int mat[3][3];

void CallBackFunc(int event,int x,int y,int flags,void* userdata);
int main(int argc,char * argv[]){

Mat img(540,540,CV_8UC3,Scalar(6,161,99));

for(int i=185;i<235;i++){
	for(int j=185;j<235;j++){
			img.at<Vec3b>(i,j)[0]= 6;
			img.at<Vec3b>(i,j)[1]= 89;
			img.at<Vec3b>(i,j)[2]= 230;	
		}
}
Point p1(190,200);
putText(img,"1.",p1,FONT_HERSHEY_SCRIPT_SIMPLEX,0.5,Scalar(0,0,0),2,1);


for(int i=255;i<305;i++){
	for(int j=185;j<235;j++){
			img.at<Vec3b>(i,j)[0]= 6;
			img.at<Vec3b>(i,j)[1]= 89;
			img.at<Vec3b>(i,j)[2]= 230;	
		}
}

Point p2(260,200);
putText(img,"2.",p2,FONT_HERSHEY_SCRIPT_SIMPLEX,0.5,Scalar(0,0,0),2,1);
putText(img,"2.",p2,FONT_HERSHEY_SCRIPT_SIMPLEX,0.5,Scalar(0,0,0),2,1);

for(int i=325;i<375;i++){
	for(int j=185;j<235;j++){
			img.at<Vec3b>(i,j)[0]= 6;
			img.at<Vec3b>(i,j)[1]= 89;
			img.at<Vec3b>(i,j)[2]= 230;	
		}
}

Point p3(330,200);
putText(img,"3.",p3,FONT_HERSHEY_SCRIPT_SIMPLEX,0.5,Scalar(0,0,0),2,1);

for(int i=185;i<235;i++){
	for(int j=255;j<305;j++){
			img.at<Vec3b>(i,j)[0]= 6;
			img.at<Vec3b>(i,j)[1]= 89;
			img.at<Vec3b>(i,j)[2]= 230;	
		}
}

Point p4(190,270);
putText(img,"4.",p4,FONT_HERSHEY_SCRIPT_SIMPLEX,0.5,Scalar(0,0,0),2,1);

for(int i=255;i<305;i++){
	for(int j=255;j<305;j++){
			img.at<Vec3b>(i,j)[0]= 6;
			img.at<Vec3b>(i,j)[1]= 89;
			img.at<Vec3b>(i,j)[2]= 230;	
		}
}
Point p5(260,270);
putText(img,"5.",p5,FONT_HERSHEY_SCRIPT_SIMPLEX,0.5,Scalar(0,0,0),2,1);

for(int i=325;i<375;i++){
	for(int j=325;j<375;j++){
			img.at<Vec3b>(i,j)[0]= 6;
			img.at<Vec3b>(i,j)[1]= 89;
			img.at<Vec3b>(i,j)[2]= 230;	
		}
}

Point p6(330,340);
putText(img,"9.",p6,FONT_HERSHEY_SCRIPT_SIMPLEX,0.5,Scalar(0,0,0),2,1);

for(int i=325;i<375;i++){
	for(int j=255;j<305;j++){
			img.at<Vec3b>(i,j)[0]= 6;
			img.at<Vec3b>(i,j)[1]= 89;
			img.at<Vec3b>(i,j)[2]= 230;	
		}
}

Point p7(330,270);
putText(img,"6.",p7,FONT_HERSHEY_SCRIPT_SIMPLEX,0.5,Scalar(0,0,0),2,1);

for(int i=255;i<305;i++){
	for(int j=325;j<375;j++){
			img.at<Vec3b>(i,j)[0]= 6;
			img.at<Vec3b>(i,j)[1]= 89;
			img.at<Vec3b>(i,j)[2]= 230;	
		}
}

Point p8(260,340);
putText(img,"8.",p8,FONT_HERSHEY_SCRIPT_SIMPLEX,0.5,Scalar(0,0,0),2,1);

for(int i=185;i<235;i++){
	for(int j=325;j<375;j++){
			img.at<Vec3b>(i,j)[0]= 6;
			img.at<Vec3b>(i,j)[1]= 89;
			img.at<Vec3b>(i,j)[2]= 230;	
		}
}
Point p9(190,340);
putText(img,"7.",p9,FONT_HERSHEY_SCRIPT_SIMPLEX,0.5,Scalar(0,0,0),2,1);




namedWindow("win");
imshow("win",img);
setMouseCallback("win",CallBackFunc,NULL);
waitKey(0);

	return 0;
}
void CallBackFunc(int event,int x,int y,int flags,void* userdata)
{


if(event == EVENT_LBUTTONDOWN){
	if(x < 235 && x > 185 && y < 235 && y > 185 )
		{
			cout << "clicked right 1.\n";
			
		}
	else if(x > 255 && x < 305 && y > 185 && y < 235){cout << "clicked right 2.\n";}
	
	else if(x > 325 && x < 375 && y > 185 && y < 235){cout << "clicked right 3.\n";}	
	
	else if(x > 185 && x < 235 && y > 255 && y < 305){cout << "clicked right 4.\n";}
	
	else if(x > 255 && x < 305 && y > 255 && y < 305){cout << "clicked right 5.\n";}
	
	else if(x > 325 && x < 375 && y > 255 && y < 305){cout << "clicked right 6.\n";}
	
	else if(x > 185 && x < 235 && y > 325 && y < 375){cout << "clicked right 7.\n";}
	
	else if(x > 255 && x < 305 && y > 325 && y < 375){cout << "clicked right 8.\n";}
	
	else if(x > 325 && x < 375 && y > 325 && y < 375){cout << "clicked right 9.\n";}
}

}