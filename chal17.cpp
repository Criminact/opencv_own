#include <iostream>
#include "include/opencv/cv.h"
#include "include/opencv2/opencv.hpp"
#include "include/opencv/highgui.h"


using namespace std;
using namespace cv;

int main(int argc,char * argv[]){

Mat img = imread("corridor.jpg");
Mat gray,edge1,edge2,edge3,edge4,edge5;
cvtColor(img,gray,CV_BGR2GRAY);

Canny(gray,edge1,50,150,3);
namedWindow("win1");
imshow("win1",edge1);

Canny(gray,edge2,100,150,3);
namedWindow("win2");
imshow("win2",edge2);

Canny(gray,edge3,20,1000,3);
namedWindow("win3");
imshow("win3",edge3);

Canny(gray,edge4,50,50,3);
namedWindow("win4");
imshow("win4",edge4);

Canny(gray,edge5,150,150,3);
namedWindow("win5");
imshow("win5",edge5);
waitKey(0);


	return 0;
}