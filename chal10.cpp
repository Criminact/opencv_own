#include <iostream>
#include "include/opencv/cv.h"
#include "include/opencv2/opencv.hpp"
#include "include/opencv/highgui.h"


using namespace std;
using namespace cv;

const int position_max = 3;
int position=0;

void classic(int position,void *);
int main(int argc,char * argv[]){


namedWindow("Dot_convert",CV_WINDOW_AUTOSIZE);
createTrackbar("DOPE","Dot_convert",&position,position_max,classic);

while(char(waitKey(1) != 'q'));

	return 0;
}
void classic(int position,void *)
{
Mat imgcvt(300,300,CV_8UC3,Scalar(40,40,40));

Point p1(150,150);
	
if(position == 1)
{
	line(imgcvt,p1,p1,Scalar(255,0,0),3);
}
else if(position == 2)
{
	line(imgcvt,p1,p1,Scalar(0,255,0),3);
}
else if(position == 3)
{
    line(imgcvt,p1,p1,Scalar(0,0,255),3);
}
imshow("Dot_convert",imgcvt);
}